﻿using UnityEngine;
using System.Collections;

public class GenerateDungeon : MonoBehaviour {
	public tk2dTileMap tileMapTutorial;

	public int dirtMaxSize = 5;
	public int dirtMinSize = 1;

	private int horizontalGroundCount = 0;
	private int verticalGroundCount = 0;

	// Use this for initialization
	void Start() {
		tileMapTutorial = GameObject.Find("TileMap0").GetComponent<tk2dTileMap>();

		// Then use SetTile to dynamically set the tile based on the input format, whether it is another tile map format like Tiled(.tmx) or even randomly generating the level
		//SetTile (int x, int y, int layer, int tile)
		//Sets the tile on a layer at x, y - either a sprite Id or -1 if the tile is empty.

		for(int y = 0; y < tileMapTutorial.height; y++) {
			for(int x = 0; x < tileMapTutorial.width; x++) {
				tileMapTutorial.SetTile(x, y, 0, -1);
			} // end for
		} // end for


		for(int y = 0; y < tileMapTutorial.height; y++) {
			for(int x = 0; x < tileMapTutorial.width; x++) {
				tileMapTutorial.SetTile(x, y, 0, WhichTile(x, y));
			} // end for
		} // end for

		// Finally call Build() on both the tile maps
		tileMapTutorial.Build();
	} // end Start()

	// Update is called once per frame
	void Update() {

	} // end Update()

	int WhichTile(int posX, int posY) {
		if(posX == 0 || posY == 0 || posX == tileMapTutorial.width - 1 || posY == tileMapTutorial.height - 1) {
			return 1;
		} // end if

		int tileIDToPick = -1;
		ArrayList tilesToSelect = new ArrayList();


		// Find how many dirt tiles occur under the current tile
		for(int count = posY; count >= 0; count--) {
			if(tileMapTutorial.GetTile(posX, count, 0) == 0) {
				verticalGroundCount++;
			} else {
				break;
			} // end if/else
		} // end for

		// Find how many dirt tiles occur to the left of the current tile
		for(int count = posX; count >= 0; count--) {
			if(tileMapTutorial.GetTile(count, posY, 0) == 0) {
				horizontalGroundCount++;
			} else {
				break;
			} // end if/else
		} // end for

		if(horizontalGroundCount <= dirtMaxSize) {
			if(!tilesToSelect.Contains(0)) {
				tilesToSelect.Add(0);
			} // end if
		} // end if
		
		if(horizontalGroundCount >= 5) {
			horizontalGroundCount = 0;
		} // end if

		if(verticalGroundCount <= dirtMaxSize) {
			if(!tilesToSelect.Contains(0)) {
				tilesToSelect.Add(0);
			} // end if
		} // end if
		
		if(verticalGroundCount >= 5) {
			verticalGroundCount = 0;
		} // end if

		print(horizontalGroundCount + ":" + verticalGroundCount);
		tilesToSelect.Add(1);

		tileIDToPick = (int)tilesToSelect[Random.Range(0, tilesToSelect.Count - 1)];

		return tileIDToPick;
	} // end WhichTile()
}
