﻿using UnityEngine;
using System.Collections;
using DungeonSharp;

public class GenerateMap : MonoBehaviour {
	public TileMap tileMap;
	public GameObject wallTile;
	public GameObject floorTile;

	public int seed = 0;
	public int cellsX = 0;
	public int cellsY = 0;
	public int cellSize = 0;


	// Use this for initialization
	void Start () {
		tileMap = GameObject.Find("TileMap").GetComponent<TileMap>();


		Grid<Tile> tiles = Generator.Generate(cellsX, cellsY, cellSize, seed);

		
		for(int y = 0; y < tiles.SizeY; y++) {
			for(int x = 0; x < tiles.SizeX; x++) {
				if(tiles[x, y] == Tile.Empty) {

				} else if(tiles[x, y] == Tile.Floor) {
					GameObject myTile = (GameObject)Instantiate(floorTile);
					myTile.transform.parent = tileMap.transform;
					myTile.transform.localPosition = new Vector3(x, 0, y);
				} else if(tiles[x, y] == Tile.Wall) {
					GameObject myTile = (GameObject)Instantiate(wallTile);
					myTile.transform.parent = tileMap.transform;
					myTile.transform.localPosition = new Vector3(x, 0, y);
				} else {

				}  // end if/else block

			} // end for

		} // end for

	}	// end Start()

	// Update is called once per frame
	void Update () {
	
	} // end Update()
}
